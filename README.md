# ShortPalindrome

**RUS**   

Дана строка. Из неё нужно сделать палиндром (когда слово читается одинаково и с начала и с конца) минимально возможной длины. Например, есть строка abcded, из нее должно получится abcdedcba.

**ENG**   

Given a string. From it you need to make a palindrome (when the word is read the same way from the beginning and from the end) of the smallest possible length. For example, there is a line abcded, from it should turn out abcdedcba.