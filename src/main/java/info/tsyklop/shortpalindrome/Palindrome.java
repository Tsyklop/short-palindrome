package info.tsyklop.shortpalindrome;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {

    public static void main(String[] args) {

        List<String> strings = new ArrayList<>();

        strings.add("abcded");
        strings.add("abcb");
        strings.add("abcdeeeeeed");

        for (String string : strings) {
            System.out.println(string + " -> " + makeShortPalindrome(string));
        }

    }

    private static String makeShortPalindrome(String word) {

        StringBuilder result = new StringBuilder(word);

        StringBuilder reversed = new StringBuilder(word).reverse();

        for (int i = 0; i < word.length() - 1; i++) {

            if (!word.substring(i).equals(reversed.toString())) {
                result.insert(result.length() - i, word.charAt(i));
                reversed.deleteCharAt(reversed.length() - 1);
            } else if (word.length() - i <= 3) {
                break;
            }

        }

        return result.toString();

    }

}
